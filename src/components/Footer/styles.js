import styled from "styled-components";

export const FooterContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: column;
    color: var(--clr-dark);
    font-weight: bold;
`

export const LinkLinkedin = styled.a`
    color: #0077B5;
    font-size: 1.5rem;
`
export const LinkGitLab = styled.a`
    color: #F48024;
    font-size: 1.5rem;
`