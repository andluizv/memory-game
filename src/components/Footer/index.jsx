import { FooterContainer, LinkLinkedin, LinkGitLab } from "./styles";
import { FaLinkedin, FaGitlab } from "react-icons/fa"

const Footer = () => {
    return (
		<FooterContainer>
			<p>Criando por André L S V.</p>
			<div>
				<LinkLinkedin
					href="https://www.linkedin.com/in/andluizv/"
					target="_blank"
					rel="noreferrer noopener"
				>
					<FaLinkedin />
				</LinkLinkedin>{" "}
				<LinkGitLab
					href="https://gitlab.com/andluizv/memory-game"
					target="_blank"
					rel="noreferrer noopener"
				>
					<FaGitlab />
				</LinkGitLab>{" "}
			</div>
		</FooterContainer>
	);
}
 
export default Footer;