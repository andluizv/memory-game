import styled from "styled-components";

export const Modal = styled.div`
    width: 100%;
    height: 100%;
    background-color: #363636cc;
    position: absolute;
    z-index: 99;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    color: var(--clr-light);
`
export const Title = styled.h2`
    font-size: 2.5rem;
`
export const BodyContent = styled.h3`
    font-size: 2.5rem;
`
export const HallOfFame = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const ButtonGroup = styled.div`
    margin-top: 15px;
    display: flex;
`

export const InputText = styled.input`
    padding: .5rem 1rem;
    text-align: center;
    border-radius: 10px 0 0 10px;
    font-family: 'Courier New', Courier, monospace;
    font-weight: bold;
`

export const SaveBtn = styled.button`
    border-radius: 0 10px 10px 0;
    padding: .5rem 1rem;
    background-color: var(--clr-medium);
    color: var(--clr-light);
    font-weight: bold;

    &:hover {
        color: var(--clr-soft-dark);
        background-color: var(--clr-soft-light);
    }
`