import { useCardsControler } from '../../Providers/CardsControler';
import { useTimer } from '../../Providers/Timer';
import { useWinControler } from '../../Providers/WinControler';
import {
	Modal,
	Title,
	BodyContent,
	InputText,
	ButtonGroup,
	SaveBtn,
	HallOfFame,
} from "./styles";
import Button from '../Button';
import { useState, useEffect } from 'react';
import api from '../../Services/api';

const VictoryModal = ({isVictory}) => {
    const {
		flipControl,
		setFlipControl,
		setRandomize,
		randomize,
		setVerifyBag,
		setFirstClick,
        setIsStarting
	} = useCardsControler();
    const { setIsVictory, setPairsFound, updateScores, setUpdateScores } = useWinControler();
    const { setTimer, stopTimer, timer } = useTimer();
    const [scoresWithoutName, setScoresWithoutName] = useState([]);
    const [scoresAPI, setScoresAPI] = useState([]);
    const [betterScore, setBetterScore] = useState(false);
    const [user, setUser] = useState("Anônimo")

    useEffect(() => {
		api
			.get()
			.then((res) => {
                setScoresAPI(Object.values(res.data))
				const victoryStatus = [user, timer];
				let testPosition = [...Object.values(res.data), victoryStatus];
				testPosition = testPosition.sort((a, b) => a[1] - b[1]);
				
				if (
					testPosition[testPosition.length - 1][1] !==
						victoryStatus[1] &&
					isVictory
				) {
					testPosition.pop();
					setScoresWithoutName(testPosition);
					setBetterScore(true);
					setUpdateScores(!updateScores);
				}
			})
			.catch((err) => console.log(err));
        
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isVictory]);

    
    const handleReset = () => {
        setFlipControl(flipControl.map(item=> item = false))
        setRandomize(!randomize)
        setIsVictory(false)
        setPairsFound([])
        setVerifyBag([]);
        setFirstClick(true);
        setIsStarting(true);
        stopTimer();
        setTimer(0);
        setBetterScore(false);
        setUser("Anônimo");
        setUpdateScores(!updateScores);
    }
    const handleSaveName = () => {
        
        const victoryStatus = [user, timer];
		let testPosition = [...scoresAPI, victoryStatus];
		testPosition = testPosition.sort((a, b) => a[1] - b[1]);
        testPosition.pop();
        api.patch("", testPosition).then(() => handleReset());
    }
    const handleSaveWithoutName = () => {
        if (betterScore){
            api
				.patch("", scoresWithoutName)
				.catch((err) => console.log(err));
        }
        handleReset();
    }
    return (
		<>
			{isVictory && (
				<Modal>
					<Title>🎉 Vitória! 🎉</Title>
					<BodyContent>
						Seu tempo foi de{" "}
						{timer >= 60 && (("00" + Math.floor(timer / 60)).slice(-2) + " minutos e")}{" "}
						{("00" + (timer % 60)).slice(-2)} segundos.
					</BodyContent>
					{betterScore && (
						<HallOfFame>
							<h3>
								Coloque seu nome no hall da fama:
							</h3>
							<ButtonGroup>
                            <InputText
								type="text"
								value={user}
								onChange={(e) => setUser(e.target.value)}
							/>
							<SaveBtn onClick={handleSaveName}>Salvar</SaveBtn>
                            </ButtonGroup>
						</HallOfFame>
					)}
					<Button onClick={handleSaveWithoutName}>
						Tentar Novamente
					</Button>
				</Modal>
			)}
		</>
	);
}
 
export default VictoryModal;