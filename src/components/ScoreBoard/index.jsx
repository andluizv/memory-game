import { useEffect, useState } from "react";
import { useWinControler } from "../../Providers/WinControler";
import { ScoreContainer, ListItemContainer, UserName, Warning } from "./styles";
import api from "../../Services/api";
const ScoreBoard = ({ showScores}) => {
    const [scores, setScores] = useState([]);
    const { updateScores } = useWinControler();
    useEffect(()=> {
        api
			.get()
			.then((res) => {
				setScores(Object.values(res.data));
			})
			.catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[updateScores])

    return (
        <>
		{showScores && (<ScoreContainer>
			<h2>Melhores tempos:</h2>
			<ol style={{ width: "100%", marginTop: "10px"}}>
				{scores.map((item) => (
					<li style={{ marginTop: "5px"}}>
                    <ListItemContainer>
							<UserName>{item[0]}</UserName>
							<span>
								{("00" + Math.floor(item[1] / 60)).slice(-2)} :{" "}
								{("00" + (item[1] % 60)).slice(-2)}
							</span>
					</ListItemContainer>
                    </li>
				))}
			</ol>
            <hr/>
            <Warning>Devido limitações do sistema Json-server, eventuamente as pontuações resetam.</Warning>
		</ScoreContainer>)}
        </>
	);
}
 
export default ScoreBoard;