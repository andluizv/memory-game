import styled from "styled-components";

export const ScoreContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: clamp(200px, 40%, 350px);
    border: 1px solid #000;
    padding: .5rem 2rem;
`

export const ListItemContainer = styled.li`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
`

export const UserName = styled.span`
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    max-width: 20ch;

    @media (max-width: 540px){
        max-width: 9ch;
    }
`
export const Warning = styled.p`
    border-top: 1px solid #000;
    margin-top: 1rem;
    text-align: center;
`

