import { useEffect, useState } from "react";
import { useCardsControler } from "../../Providers/CardsControler";
import { useTimer } from "../../Providers/Timer";
import { useWinControler } from "../../Providers/WinControler";
import Card from "../Card";
import VictoryModal from "../VictoryModal";
import {
	Container,
	CardsContainer,
	Separator,
	InputsContainer,
	Checkbox,
} from "./styles";
import Button from "../Button";

const Table = ({ showScores, setShowScores}) => {
	const [arr, setArr] = useState([]);
	const { isVictory, setPairsFound } = useWinControler();
    
	const {
		randomize,
		setRandomize,
		flipControl,
		setFlipControl,
		setVerifyBag,
		setFirstClick,
        setIsStarting
	} = useCardsControler();
    const { timer, setTimer, stopTimer } = useTimer();

	useEffect(() => {
		let ex = [
			1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 10, 10, 9, 9, 11, 11,
		];
		setArr(ex.sort(() => 0.5 - Math.random()));
		// setArr(ex);
	}, [randomize]);

	const handleReset = () => {
		setFlipControl(flipControl.map((item) => (item = false)));
		setRandomize(!randomize);
		setVerifyBag([]);
		setPairsFound([]);
		setFirstClick(true);
        setIsStarting(true);
        stopTimer();
        setTimer(0);
	};

    
	return (
		<Container>
			<VictoryModal isVictory={isVictory} />
			<CardsContainer>
				{arr.map((item, ind) => (
					<Card imgId={item} controlId={ind} key={`${ind}${item}`} />
				))}
			</CardsContainer>
			<Separator>
				<Button onClick={handleReset}>Reiniciar</Button>
				<p>
					Timer⏰ {("00" + Math.floor(timer / 60)).slice(-2)} :{" "}
					{("00" + (timer % 60)).slice(-2)}
				</p>
				<InputsContainer>
					<Checkbox
						type="checkbox"
						name="ScoreBoard"
						checked={showScores}
						onChange={setShowScores}
					/>
					<label htmlFor="ScoreBoard">Mostrar melhores tempos</label>
				</InputsContainer>
			</Separator>
		</Container>
	);
};

export default Table;
