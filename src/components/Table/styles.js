import styled from "styled-components";

export const Container = styled.div`
    width: clamp(320px, 90%, 1200px);
    height: auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    gap: 10px;
    position: relative;
    padding: 1.5rem;
    border: 1px solid black;
    background-color: var(--clr-soft-light);
    flex-grow: 0;
`

export const CardsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    gap: 10px;
`

export const Separator = styled.div`
    display: flex;
    flex-wrap: wrap;
    gap: 20px;
    align-items: center;
    justify-content: center;
    width: 100%;
    border-top: 1px solid #000;
    padding-top: 1rem;
    margin-top: 1rem;
`

export const InputsContainer = styled.div`
    display: flex;
    align-items: center;
    gap: 10px;
`
export const Checkbox = styled.input`
	&::before {
		content: " ";
		width: inherit;
		height: inherit;
		position: absolute;
		background-color: var(--clr-light);
		border: 1px solid #000;
	}
	&:checked::after {
		content: " ";
		width: 0.2rem;
		height: 0.5rem;
		margin-left: 0.3rem;
		border-color: var(--clr-soft-dark);
		border-style: solid;
		border-width: 0 3px 4px 0;
		position: absolute;
		transform: rotate(45deg);
	}
`;