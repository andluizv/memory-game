import {
	Container,
	BackView,
	ImageContainer,
	FrontView,
	CardPattern,
} from "./styles";
import { useRef,} from "react";
import { useCardsControler } from "../../Providers/CardsControler";
import CardBackPattern from "../../Images/jupiter.svg"
import { useWinControler } from "../../Providers/WinControler";
import { useTimer } from "../../Providers/Timer";

const Card = ({imgId, controlId, timer}) => {
    const {
		flipControl,
		setFlipControl,
		setVerifyBag,
		verifyBag,
		setFirstClick,
		firstClick,
		setIsStarting,
		isStarting,
		
	} = useCardsControler();
    const { pairsFound, setPairsFound } = useWinControler();
    const {startTimer} = useTimer();
    const ref = useRef();

    const handleFlip = () => {
        if (isStarting){
            setIsStarting(false)
            startTimer();
        }
        if (firstClick) {
            let tempObj = {}
            tempObj[imgId] = controlId
            setVerifyBag(tempObj)
            let tempArr = flipControl;
            tempArr[controlId] = true
            setFlipControl(tempArr);
            setFirstClick(false)
            return;
        
        } else {
            if (Object.keys(verifyBag).includes(imgId.toString())) {
                let tempArr = flipControl;
				tempArr[controlId] = true;
				setFlipControl(tempArr);
                setFirstClick(true);
                setVerifyBag([]);
                setPairsFound([...pairsFound, imgId])
                return;
                
		    }
            
            setTimeout(() => {
                ref.current.toggle();
                
            }, 700);
            setTimeout(() => {
                let tempArr = flipControl;
				tempArr[controlId.toString()] = false;
				tempArr[Object.values(verifyBag)[0].toString()] = false;
				setFlipControl(tempArr);
            }, 200)   
            setVerifyBag([]);
            setFirstClick(true);
        }
        
        
    }
    

    return (
		<Container isFlipped={flipControl[controlId.toString()]} ref={ref}>
			<FrontView onClick={handleFlip}>
				<CardPattern src={CardBackPattern} />
			</FrontView>
			<BackView onClick={()=> {ref.current.setFlipped(true)}}>
				<ImageContainer
					src={`https://loremflickr.com/320/240/illustration?lock=${imgId}`}
					alt={`FigureId ${imgId}`}
				/>
			</BackView>
		</Container>
	);
}
 
export default Card;