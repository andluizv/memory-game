import styled from "styled-components";
import Flippy, { FrontSide, BackSide } from "react-flippy";

export const Container = styled(Flippy)`
	width: clamp(50px, 10vw, 150px);
	height: clamp(50px, 10vw, 150px);
    pointer-events: ${props => props.isFlipped ? 'none' : 'auto'};
	display: grid;
	place-items: center;
`;

export const ImageContainer = styled.img`
	width: 100%;
	border-radius: 41% 59% 28% 72% / 61% 40% 60% 39%;
	object-fit: fill;
`;

export const FrontView = styled(FrontSide)`
	width: 100%;
	height: 100%;
	display: grid;
	place-items: center;
	border: 1px solid black;
	background-color: var(--clr-medium);
`;

export const BackView = styled(BackSide)`
	border: 1px solid black;
	background-color: var(--clr-soft-dark);
	width: 100%;
	height: 100%;
	display: grid;
	place-items: center;
`;

export const CardPattern = styled.img`
	width: 95%;
	height: 95%;
	border: clamp(2px, .2vw, 10px) solid var(--clr-dark);
`;