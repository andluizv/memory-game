import Table from "../../components/Table";
import ScoreBoard from "../../components/ScoreBoard";
import { useState } from "react";
import { Title } from "./styles";
import Footer from "../../components/Footer";

const HomePage = () => {
    const [showScores, setShowScores] = useState(false);

    const handleChange = () => {
		setShowScores(!showScores);
	};

    return (
		<>
			<Title> Jogo da Memória 🧠</Title>
			<Table showScores={showScores} setShowScores={handleChange} />
			<ScoreBoard showScores={showScores} />
            <Footer />
		</>
	);
}
 
export default HomePage;