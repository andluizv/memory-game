import styled from "styled-components";

export const Title = styled.h1`
    font-size: 3rem;
    font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
    color: var(--clr-soft-dark);
    text-align: center;
`