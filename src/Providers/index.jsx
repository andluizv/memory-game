import { CardsControlerProvider } from "./CardsControler";
import { WinControlerProvider } from "./WinControler";
import { TimerProvider } from "./Timer";

const Providers = ({ children }) => {
	return (
		<CardsControlerProvider>
			<TimerProvider>
				<WinControlerProvider>{children}</WinControlerProvider>
			</TimerProvider>
		</CardsControlerProvider>
	);
};

export default Providers;
