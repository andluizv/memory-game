import { useContext, useEffect, useState } from "react";
import { createContext } from "react";
import { useTimer } from "../Timer";

const WinControlerContext = createContext();

export const WinControlerProvider = ({ children }) => {
	const [pairsFound, setPairsFound] = useState([])
    const [isVictory, setIsVictory] = useState(false);
    const { stopTimer } = useTimer();
    const [updateScores, setUpdateScores] = useState(false);
    useEffect(() => {
        if (pairsFound.length === 10){
            setIsVictory(true)
            stopTimer();
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[pairsFound])
	return (
		<WinControlerContext.Provider
			value={{
				pairsFound,
				setPairsFound,
				isVictory,
				setIsVictory,
				updateScores,
				setUpdateScores,
			}}
		>
			{children}
		</WinControlerContext.Provider>
	);
};

export const useWinControler = () => useContext(WinControlerContext);
