import { useContext, useRef, useState } from "react";
import { createContext } from "react";

const TimerContext = createContext();

export const TimerProvider = ({ children }) => {
	const [timer, setTimer] = useState(0);
	const timerControler = useRef(null);

    const startTimer = () => {
        timerControler.current = setInterval(() => {
			setTimer((timer) => timer + 1);
		}, 1000);
    }
    const stopTimer = () => {
        clearInterval(timerControler.current)
    }
	return (
		<TimerContext.Provider
			value={{ timer, setTimer, startTimer, stopTimer }}
		>
			{children}
		</TimerContext.Provider>
	);
};

export const useTimer = () => useContext(TimerContext);
