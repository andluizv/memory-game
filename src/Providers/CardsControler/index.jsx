import { useContext, useEffect, useState } from "react";
import { createContext } from "react";

const CardsControlerContext = createContext();

export const CardsControlerProvider = ({ children }) => {
	const [flipControl, setFlipControl] = useState([false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]);
    const [verifyBag, setVerifyBag] = useState([]);
    const [firstClick, setFirstClick] = useState(true);
    const [isStarting, setIsStarting] = useState(true);
	const [randomize, setRandomize] = useState(false);
    useEffect(()=>{
        setFlipControl(flipControl);
    },[flipControl])
    return (
		<CardsControlerContext.Provider
			value={{
				flipControl,
				setFlipControl,
				verifyBag,
				setVerifyBag,
				firstClick,
				setFirstClick,
				isStarting,
				setIsStarting,
				randomize,
				setRandomize,
			}}
		>
			{children}
		</CardsControlerContext.Provider>
	);
};

export const useCardsControler = () => useContext(CardsControlerContext);
