import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Providers from "./Providers";
import GlobalStyles from "./Styles/global";

ReactDOM.render(
	<React.StrictMode>
		<GlobalStyles />
		<Providers>
			<App />
		</Providers>
	</React.StrictMode>,
	document.getElementById("root")
);
