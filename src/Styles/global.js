import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
        font-family: "Inter", sans-serif;
    }

    :root{
        --clr-light: hsl(52, 50%, 91%);
        --clr-soft-light: #b7d7b9;
        --clr-medium: #3c6f72;
        --clr-soft-dark: #284b62;
        --clr-dark: #363636;
    }
    html {
        width: 100%;
        min-height: 100%;
    }
    a {
        text-decoration: none;
    }
    button, input {
        outline: none;
        border: none;
    }
    button {
        cursor: pointer;
    }
    body {
        background-color: var(--clr-light);
        height: 100%;
        max-width: 100vw;
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        flex-grow: 0;
        justify-content: center;
        align-items: center;
    }
    #root{
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        flex-wrap: wrap;
        gap: 10px;
        margin-top: 1rem;
    
    }

    .flippy-cardContainer .flippy-front, .flippy-back {
        padding: 0;
    }
`;
